<?php
require_once('../config.php');
// conexão banco de dados mysql (Simples)
    // $host = "localhost";
    // $usuario = "root";
    // $senha = "";
    // $banco = "dinamicodb";
    //$db = mysqli_connect($host, $usuario,$senha, $banco);
    // PDO - PHP DATA OBJECT
     $sql = "mysql:host=".IP_SERVER_DB.";dbname=".NOME_BANCO;;
     $dns_opt = [PDO::ATTR_ERRMODE=>PDO::ERRMODE_EXCEPTION];
     try {
         //bloco de tratamento de exceção
        $conn = new PDO($sql,USER_DB,PASS_DB,$dns_opt);
        //echo "Conectado com sucesso.";
     } catch (PDOException $error) {
         echo "Erro de conexão: ".$error->getMessage();
     }
  
 
?>
<?php
require_once('conexao.php');
 $query = "select * from categoria";
 $cmd = $conn->prepare($query);
 $cmd->execute();
 $categoria_retornado = $cmd->fetchAll(PDO::FETCH_ASSOC);
 if (count($categoria_retornado)>0) {
   

    //var_dump($categoria_retornado);

?>

<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Listagem</title>
    <link rel="stylesheet" href="../css/style_admin.css">
</head>
<body>
    <table width="100%" border="0" cellpadding = "0" cellspacing="1" bgcolor="#660000">
        <tr bgcolor="#993300" align="center">
            <td width="15%" heigth="2"><strong><font size="2" color="#fff">Código</font></strong></td>
            <td width="60%" heigth="20"><strong><font size="2" color="#fff">Categoria</font></strong></td>
            <td width="15%" heigth="20"><strong><font size="2" color="#fff">Ativo</font></strong></td>
            <td colspan="2" heigth="20"><strong><font size="2" color="#fff">Opcões</font></strong></td>
        </tr>
    <?php 
      foreach ($categoria_retornado as $categoria) {
        
    ?>
        <tr bgcolor="#fff">

            <td><font size="2" face="verdana,arial"><?php echo $categoria['id_categoria']; ?></font></td>
            <td><font size="2" face="verdana,arial"><?php echo $categoria['categoria']; ?></font></td>
            <td><font size="2" face="verdana,arial"><?php echo $categoria['cat_ativo']; ?></font></td>
            <td align="center"><font size="2" face="verdana,arial"><a href="principal.php?link=">Alterar</a></font></td>
            <td align="center"><font size="2" face="verdana,arial"><a href="principal.php?link=">Excluir</a></font></td>
    <?php   }} ?>
        </tr>
    </table>
</body>
</html>
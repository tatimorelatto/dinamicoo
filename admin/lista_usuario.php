<?php
require_once('conexao.php');
// verificar se o usuário clicou no botão cadastrar 
if (isset($_POST['cadastro'])) {
    //recuperar dados dos campos do form
    $nome = $_POST['nome'];
    $email = $_POST['email'];
    $foto = $_FILES['foto'];

    if (!empty($foto['name'])){
        //largura maxima em pixels
        $largura = 6000;
         //altura maxima em pixels
         $altura = 4000;
         //tamanho maximo em byte 
         $tamanho = 4048576;

         $error = array();


         //verifica se é uma imagem
         if(!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp)$/",$foto['type'])){
             $error[1] = "Isso não é uma imagem!";
             
         }
         //recuperando as dimensões
         $dimensões = getimagesize($foto['tmp_name']);
         //verifica se a largura da imagem é maior que a permitida
         if($dimensões[0]>$largura){
            $error[2] = " A lagura da imagem é maior que a permitida! Não deve ultrapassar".$largura."pixels.";

         }
         //verifica se a altura da imagem é maior que a permitida

         if($dimensões[0]>$altura){
            $error[3] = " A altura da imagem é maior que a permitida! Não deve ultrapassar".$altura."pixels.";;
            
         }
         //verifica se o tamanho da imagem é maior que a permitida
         if($foto['size']>$tamanho){
            $error[4] = " O tamanho da imagem é maior que  permitida! Não deve ultrapassar".$tamanho."bytes.";;
         }

         //se não houver erros 
         if(count($error)==0){
             //recupera a extensão do arquivo de imagem
             preg_match("/\.(gif|bmp|png|jpg){1}$/i",$foto['name'],$ext);  //vai na foto que escolheu e pega apenas a extensão e armazena na variavel ext

             // gera um nome de arquivo unico para a imagem
             $nome_imagem = md5(uniqid(time())).".".$ext[1];
             // caminho para armazenar as imagens
             $caminho_imagem = "foto/".$nome_imagem;
             //realiza o upload da imagem a partir do espaço temporario
             move_uploaded_file($foto['tmp_name'],$caminho_imagem);
             //insere os dados dos usuarios
              $cmd = $conn->prepare("insert into usuario (nome, email, foto) values(:nome, :email, :foto)");
              $result = $cmd->execute(array(
                  ":nome"=>$nome,
                  ":email"=>$email,
                  ":foto"=>$nome_imagem                  
              ));  
              //var_dump($result);

              echo "<br> Usuário inserido com sucesso.";

         }
            

    }
    if(count($error)!=0){
        foreach ($error as $erro) {
            echo $erro."<br>";
        }

    }

}
//tratar exibição
    echo"Usuarios Cadastrados!"."<br>";
    
   
        $cmd = $conn->prepare("select * from usuario");
        $cmd->execute();
        $result = $cmd->fetchALL(PDO::FETCH_ASSOC);
        //var_dump($result);
        foreach ($result as $usuario) {
            echo"<br>";
            echo "<img src='foto/".$usuario['foto']."' width='86px' heigh='58'>";
            echo"<br>";
            echo"Nome: ".$usuario['nome'];
            echo"<br>";
            echo"Email: ".$usuario['email'];
            echo"<br>";
            
        }

      
    ?>

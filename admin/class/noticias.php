<?php
class Noticias{
    public $id_noticia;
    public $id_categoria;
    public $titulo_noticia;
    public $img_noticia;
    public $visita_noticia;
    public $data_noticia;
    public $noticia_ativo;
    public $noticia;
    


    public static function loadById($id_not){
        $sql = new Sql(); 
        $results = $sql->select("SELECT * FROM noticia WHERE id_noticia = :id_noticia",
        array(":id_noticia"=>$id_not));
        if (count($results)>0){
            //self::setData($results[0]);
            return $results[0];
        }
    }

    public static function getList(){
        $sql = new Sql();
        return $sql->select("SELECT * FROM noticia ORDER BY id_noticia");  //order by diz qual a ordem de exibição
    }

    public static function search($not){ //função search
        $sql = new Sql();
        return $sql->select("SELECT * FROM noticia WHERE titulo_noticia LIKE :titulo_noticia",
                array(":nome"=>"%".$not."%"));
    

    }

    public static function setData($data){ //pega as info que retornaram do banco e associa aos atributos da classe
        $id_noticia= $data['id_noticia'];
        $id_categoria = $data['id_categoria'];
        $titulo_noticia= $data['titulo_noticia'];
        $img_noticia = $data['img_noticia'];
        $visita_noticia= $data['visita_noticia'];
        $data_noticia = $data['data_noticia'];
        $noticia_ativo= $data['noticia_ativo'];
        $noticia = $data['noticia'];
        
    }

    public function insert(){
        $sql = new Sql();
        $results = $sql->select("CALL sp_noticia_insert(:id_noticia, :id_categoria, :titulo_noticia, :img_noticia, :visita_noticia, :data_noticia, :noticia_ativo, :noticia)",   //procedure
            array(
                ":id_noticia"=>$this->id_noticia,
                ":id_categoria"=>$this->id_categoria,
                ":titulo_noticia"=>$this->titulo_noticia,
                ":img_noticia"=>$this->img_noticia,
                ":visita_noticia"=>$this->visita_noticia,
                ":data_noticia"=>$this->data_noticia,
                ":noticia_ativo"=>$this->noticia_ativo,
                ":noticia"=>$this->noticia

           ));
        if(count($results)>0){
            //$this->setData($results[0]);
            return $results[0];
            
        }
    }

    public function update($_id_noticia,$_id_categoria, $_titulo_noticia, $_img_noticia, $_visita_noticia, $_data_noticia, $_noticia_ativo, $_noticia){
        //$this->setNome($_nome);
        //$this->setSenha($_senha);
        $sql = new Sql();
        $sql->query("UPDATE noticia SET  id_categoria = :id_categoria, titulo_noticia = :titulo_noticia, img WHERE id_noticia = :id_noticia",
            array(
                         
                ":id_noticia"=>$_id_noticia,
                ":id_categoria"=>$_id_categoria,
                ":titulo_noticia"=>$_titulo_noticia,
                ":img_noticia"=>$_img_noticia,
                ":visita_noticia"=>$_visita_noticia,
                ":data_noticia"=>$_data_noticia,
                ":noticia_ativo"=>$_noticia_ativo,
                ":noticia"=>$_noticia
            ));
    }

    public function delete(){
        $sql = new Sql();
        $sql->query("DELETE FROM noticia WHERE id_noticia = :id_noticia", 
        array(":id_noticia"=>$this->id_noticia));
    }
    // criando métodos construtores no PHP
    public function __construct($id_categoria="", $titulo_noticia="", $img_noticia="", $visita_noticia="", $data_noticia="", $noticia_ativo="", $noticia=""){
        
        $this->id_categoria=$id_categoria;
        $this->titulo_noticia=$titulo_noticia;
        $this->img_noticia=$img_noticia;
        $this->visita_noticia=$visita_noticia;
        $this->data_noticia=$data_noticia;
        $this->noticia_ativo=$noticia_ativo;
        $this->noticia=$noticia;
    }


    public function __toString(){
        return json_encode(array(
             
                    ":id_noticia"=>$this->id_noticia,
                    ":id_categoria"=>$this->id_categoria,
                    ":titulo_noticia"=>$this->titulo_noticia,
                    ":img_noticia"=>$this->img_noticia,
                    ":visita_noticia"=>$this->visita_noticia,
                    ":data_noticia"=>$this->data_noticia,
                    ":noticia_ativo"=>$this->noticia_ativo,
                    ":noticia"=>$this->noticia
        ));
    }
    
}
?>
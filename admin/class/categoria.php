<?php
class Categoria{
    public $id_categoria;
    public $categoria;
    public $cat_ativo;

    public static function loadById($id_cat){
        $sql = new Sql(); 
        $results = $sql->select("SELECT * FROM categoria WHERE id_categoria = :id_categoria",
        array(":id"=>$id_cat));
        if (count($results)>0){
            return $results[0];
        }
    }

    public static function getList(){
        $sql = new Sql();
        return $sql->select("SELECT * FROM categoria ORDER BY id");  //order by diz qual a ordem de exibição
    }

    public static function search($cat){ //função procurar
        $sql = new Sql();
        return $sql->select("SELECT * FROM categoria WHERE nome LIKE :nome",
                array(":nome"=>"%".$cat."%"));

    }

    
    public static function setData($data){ //pega as info que retornaram do banco e associa aos atributos da classe
        $id_categoria= $data['id_categoria'];
        $categoria = $data['categoria'];
        $cat_ativo= $data['cat_ativo'];
     
    }

    public function insert(){
        $sql = new Sql();
        $results = $sql->select("CALL sp_cat_insert(:categoria, :cat_ativo)",   //procedure
            array(
                ":categoria"=>$this->categoria,
                ":cat_ativo"=>$this->cat_ativo
                               
            ));
        if(count($results)>0){
            //$this->setData($results[0]);
            return $results[0];
            
        }
    }

    public function update($_id,$_categoria, $_cat_ativo){
        //$this->setNome($_nome);
        //$this->setSenha($_senha);
        $sql = new Sql();
        $sql->query("UPDATE categoria SET categoria = :categoria, cat_ativo = :cat_ativo WHERE id_categoria = :id_categoria",
            array(
                ":id_categoria"=>$_id,
                ":categoria"=>$_categoria,
                ":cat_ativo"=>$_cat_ativo,
                
            ));
    }

    public function delete(){
        $sql = new Sql();
        $sql->query("DELETE FROM categoria WHERE id_cat = :id_cat", 
        array(":id_cat"=>$this->id_cat));
    }
    // criando métodos construtores no PHP
    public function __construct($categoria="",$cat_ativo=""){
        $this->categoria=$categoria;
        $this->cat_ativo=$cat_ativo;
        }

    public function __toString(){
        return json_encode(array(
            "id_categoria"=>$this->id_categoria,    
            "categoria"=>$this->categoria,
            "cat_ativo"=>$this->cat_ativo,
          
        ));
    }
    

} 
?>
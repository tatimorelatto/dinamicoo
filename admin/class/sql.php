<?php
class Sql extends PDO{
        private $cn;
        public function __construct(){
            $this->cn = new PDO("mysql:host=localhost;dbname=dinamicodb","root","");
        }
        private function setParams($comando, $parametros=array()){
            foreach ($parametros as $key => $value){
                $this->setParam($comando, $key, $value);
            }
        }
        public function setParam($comando, $key, $value){
            $comando->bindParam($key, $value);
        }
        public function query($comandoSql, $params = array()){
            $cmd = $this->cn->prepare($comandoSql);
            $this->setParams($cmd, $params);
            $cmd->execute();
            return $cmd;
        }
        public function select($comandoSql, $params = array()){
            $cmd = $this->query($comandoSql, $params);
            return $cmd->fetchAll(PDO::FETCH_ASSOC);
        }
    } 

?>
<?php
class Banner{
    public $id_banner;
    public $titulo_banner;
    public $link_banner;
    public $img_banner;
    public $alt;
    public $banner_ativo;


    public static function loadById($id_ban){
        $sql = new Sql(); 
        $results = $sql->select("SELECT * FROM banner WHERE id = :id",
        array(":id"=>$id_ban));
        if (count($results)>0){
            //self::setData($results[0]);
            return $results[0];
        }
    }

    public static function getList(){
        $sql = new Sql();
        return $sql->select("SELECT * FROM banner ORDER BY nome");
    }

    public static function search($ban){
        $sql = new Sql();
        return $sql->select("SELECT * FROM banner WHERE nome LIKE :nome",
                array(":nome"=>"%".$ban."%"));
    }

 
    public static function setData($data){
        $id= $data['id_banner'];
        $titulo_banner = $data['titulo_banner'];
        $link_banner= $data['link_banner'];
        $img_banner= $data['img_banner'];
        $alt = $data['alt'];
        $banner_ativo = $data['banner_ativo'];
    }

    public function insert(){
        $sql = new Sql();
        $results = $sql->select("CALL sp_ban_insert(:titulo_banner, :link_banner, :img_banner, ?alt, :banner_ativo)",
            array(
                ":titulo_banner"=>$this->titulo_banner,
                ":link_banner"=>$this->link_banner,
                ":img_banner"=>$this->img_banner,
                ":alt"=>$this->alt,
                ":banner_ativo"=>$this->banner_ativo
            ));
        if(count($results)>0){
            //$this->setData($results[0]);
            return $results[0];
        }
    }

    public function update($_id_banner,$_link_banner,$_img_banner,$_alt,$_banner_ativo){
        //$this->setNome($_nome);
        //$this->setSenha($_senha);
        $sql = new Sql();
        $sql->query("UPDATE banner SET link_banner = :link_banner, img_banner = :img_banner, alt = :alt, banner_ativo = :banner_ativo  WHERE id = :id",
            array(
                ":id_banner"=>$_id_banner,
                ":link_banner"=>$_link_banner,
                ":img_banner" =>$_img_banner,
                ":alt" =>$_alt,
                ":banner_ativo" => $_banner_ativo
            ));
    }

    public function delete(){
        $sql = new Sql();
        $sql->query("DELETE FROM banner WHERE id = :id", 
        array(":id"=>$this->id));
    }
    // criando métodos construtores no PHP
    public function __construct($link_banner="",$img_banner="",$alt="", $banner_ativo=""){
        $this->link_banner=$link_banner;
        $this->img_banner=$img_banner;
        $this->alt = $alt;
        $this->banner_ativo = $banner_ativo;
        
    }

    public function __toString(){
        return json_encode(array(
            "id_banner"=>$this-> $id_banner,
            ":titulo_banner"=>$this->titulo_banner,
            ":link_banner"=>$this->link_banner,
            ":img_banner"=>$this->img_banner,
            ":alt"=>$this->alt,
            ":banner_ativo"=>$this->banner_ativo   
            
        ));
    }
    
}

?>